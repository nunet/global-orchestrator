import os
import sys
import time
import grpc
import pickle
import log

from concurrent import futures
from pprint import pprint

import service_pb2_grpc
import service_pb2
# import utils


if len(sys.argv) == 2:
    grpc_port = sys.argv[1]
else:
    grpc_port = "4556"


deployment_type = os.environ['deployment_type']

tokenomics_address = os.environ['tokenomics_address']

tokenomics_port_test = os.environ['tokenomics_port_test']

tokenomics_port_prod = os.environ['tokenomics_port_prod']


if deployment_type == "test":
    tokenomics_port = tokenomics_port_test
    # from jobs import run_jobs

else:
    tokenomics_port = tokenomics_port_prod
    # from jobs import run_jobs_prod as run_jobs

# from jobs import acl_jobs

logger = log.setup_custom_logger('TRACE')

class Registry(service_pb2_grpc.RegistryServicer):
    def __init__(self):
        self.registry = {
            "node_ids": [],
            "peer_addrs": [],
            "peer_meta": [],
            "tokenomics": [str(tokenomics_address), str(tokenomics_port)],
        }
        self.temp = 0

    def updateRegistry(self, request, context):
        while self.temp == 0:
            services_info = request.services_info
            services_info = pickle.loads(services_info)
            res = service_pb2.Empty()
            services_info["timestamp"]=int(time.time())
            if services_info == {}:
                return res
            elif services_info["peer_id"]["nodeID"] in self.registry["node_ids"]:
                index = self.registry["node_ids"].index(
                    services_info["peer_id"]["nodeID"])
                self.registry["peer_meta"][index] = services_info
                self.registry["peer_addrs"][index] = services_info["ip_addrs"]
                return res
            else:
                self.registry["peer_addrs"].append(services_info["ip_addrs"])
                self.registry["node_ids"].append(services_info["peer_id"]["nodeID"])
                self.registry["peer_meta"].append(services_info)
                return res

    def reqServiceEndpoints(self, request, context):
        self.temp = 1
        try:
            address = {}
            peer_ids = []
            endpoints = []
            service_name = request.service_name
            if "tokenomics" in service_name:
                address["endpoints"] = self.registry["tokenomics"]
                address["peer_ids"] = []
                results = pickle.dumps(address)
                return service_pb2.ServiceEndpoints(endpoints=results)
            elif "slave" in service_name:
                node_id = service_name.split(" ")[1]
                if node_id in self.registry["node_ids"]:
                    index = self.registry["node_ids"].index(node_id)
                    endpoints.append(self.registry["peer_addrs"][index])
                    peer_ids.append(self.registry['peer_meta'][index]['peer_id'])
                    address["peer_ids"] = peer_ids
                    address["endpoints"] = endpoints
                    results = pickle.dumps(address)

                    return service_pb2.ServiceEndpoints(endpoints=results)


            else:
                for peer_info in self.registry["peer_meta"]:
                    for service in peer_info["services"]:
                        logger.info(peer_info)
                        if request.service_name in service["name"] :
                            endpoints.append(peer_info["ip_addrs"])
                            peer_ids.append(peer_info["peer_id"])
                address["peer_ids"] = peer_ids
                address["endpoints"] = endpoints
                results = pickle.dumps(address)

                return service_pb2.ServiceEndpoints(endpoints=results)
        except Exception as e:
            logger.error(e)
        finally:
            self.temp = 0

    # def runService(self, request, context):
    #     service_name=request.service_name
    #     user_name=request.user_name
    #     logger.info(service_name)
    #     logger.info(user_name)
    #     token=acl_jobs.get_token(service_name,user_name)
    #     logger.info(token)
    #     res = service_pb2.Token()
    #     res.token=str(token)
    #     return res


def main():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=100))
    service_pb2_grpc.add_RegistryServicer_to_server(Registry(), server)
    server.add_insecure_port('[::]:'+str(grpc_port))
    server.start()
    try:
        while True:
            time.sleep(10)
    except KeyboardInterrupt:
        server.stop(0)



if __name__ == '__main__':
    main()
