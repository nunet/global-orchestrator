FROM ubuntu:18.04

RUN apt-get update 

RUN apt-get update && apt-get install --no-install-recommends  -y python3.7 python3-pip
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1


RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools

COPY requirements.txt /

RUN pip3 install -r /requirements.txt
RUN apt-get update
RUN apt-get install build-essential -y
RUN apt install libpython3.7-dev -y

ENV tokenomics_address=135.181.222.170

ENV tokenomics_port_test=4557

ENV tokenomics_port_prod=4556

EXPOSE 4556
ENV NOMAD_ADDR=http://nomad.nunet.io:4646
RUN apt-get update -y
RUN apt-get install curl -y
RUN apt install libcurl4-openssl-dev libssl-dev -y
COPY . /REGISTRY 
WORKDIR /REGISTRY


RUN sh buildproto.sh

CMD ["python3", "main.py"]
