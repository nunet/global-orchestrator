import nomad


def query_nomad(service_name):
    addresses = []
    n = nomad.Nomad(host='nomad.nunet.io')
    j = n.jobs[service_name]["ID"]

    maxv = 0
    i = 0
    c = -1
    if len(n.job.get_allocations(j)) == 0:
        return []
    for allocs in n.job.get_allocations(j):
        c += 1
        if allocs["JobVersion"] >= maxv:
            i = c
            maxv = allocs["JobVersion"]
    task_id = n.job.get_allocations(j)[i]["ID"]
    service_allocations = n.job.get_allocations(j)
    index = 0
    for i, alloc in enumerate(service_allocations):
        if alloc["ID"] == task_id:
            index = i
    service_allocations[0], service_allocations[index] = service_allocations[index], service_allocations[0]
    for alloc in service_allocations:
        res = get_alloc(n, alloc["ID"])
        results = parse_allocation(res, service_name)
        for result in results:
            addresses.append(result)
    return addresses


def parse_allocation(allocation, service_name):
    try:
        networks = allocation['TaskResources'][service_name]['Networks']
    except Exception as e:
        return []
    else:
        if (networks):
            allocation_addrs = []
            for network in networks:
                ip = network['IP']
                if (network['DynamicPorts'] == None):
                    ports = network['ReservedPorts']
                elif (network['ReservedPorts'] != None and network['DynamicPorts'] != None):
                    ports = []
                    ports.extend(network['ReservedPorts'])
                    ports.extend(network['DynamicPorts'])
                else:
                    ports = network['DynamicPorts']
                for port in ports:
                    allocation_addrs.append((ip, port['Value']))

            return allocation_addrs


def get_alloc(nomad, id):
    allocation = nomad.allocation.get_allocation(id)
    return allocation
