import nomad

from jobs import run_jobs_prod as run_jobs
import json
import subprocess
import logging

n = nomad.Nomad(host="172.16.100.10",timeout=10)

acl_job=json.load(open("./jobs/acl_policy.json","r"))

def get_token(service_name,user_name):
    #check if service name is cardano relay
    if "cardano" in service_name and "relay" in service_name:
        nodes=n.nodes.get_nodes()
        for node in nodes:
            if node["Status"]=="ready": #and node["Datacenter"]=="nunet-private-alpha":
                print(node["Datacenter"])
                node_name=node["Name"]
                total_alloc,adapter_running,passive_node=run_jobs.total_allocs(node_name)
                adapter_running=True
                if adapter_running:
                    print(total_alloc)
                    #get resources
                    node_res=n.node.get_node(node["ID"])["Resources"]
                    memory_total=node_res["MemoryMB"]
                    cpu_total=node_res["CPU"]
                    node_res=n.node.get_node(node["ID"])["Reserved"]
                    memory_reserved=node_res["MemoryMB"]
                    cpu_reserved=node_res["CPU"]
                    memory_available=memory_total-memory_reserved
                    cpu_available=cpu_total-cpu_reserved

                    if cpu_available >= 4000 and memory_available >= 4000:
                        #create namespace
                        namespace_name=service_name+user_name+node_name
                        namespace_name=namespace_name.replace("_","-")
                        namespace={"Namespace":namespace_name,"Description":"cardano relay node"}
                        
                        output=subprocess.check_output( ["nomad" ,"namespace", "apply" ,"-description", "cardano relay node",  str(namespace_name)])
                        print(output)

                        #write acl policy for the namespace
                        acl_job["namespace"]={'*': {'policy': 'deny'}, namespace_name: {'policy': 'write'}}
                        
                        acl_policy="jobs/acl_policies/"+namespace_name+'.json'
                        acl_policy=acl_policy.replace(" ","_")
                        with open(acl_policy, 'w') as file:
                            json.dump(acl_job, file)

                        #create the generated acl policy

                        output=subprocess.check_output( ["nomad" ,"acl" ,"policy", "apply", "-description", "cardano relay node policy", str(namespace_name), str(acl_policy)])

                        print(output)
                        #generate acl token for the genreated policy 
                        new_token = {
                            "Name": "Readonly token",
                            "Type": "client",
                            "Policies": [namespace_name],
                            "Global": True
                        }

                        created_token = n.acl.create_token(new_token)
                        

                        #deploy job
                        deploy_job(service_name,namespace_name,node_name)

                        return created_token

def deploy_job(service_name,namespace,node_name):
    node_name_constraint=node_name
    node_name=node_name.replace(" ","_")
    #cardano_node
    cardano_node=json.load(open("./jobs/cardano_node_passive_prod.json","r"))
    cardano_node["Job"]["Name"]="cardano-node-"+node_name
    cardano_node["Job"]["Namespace"]=namespace
    cardano_node["Job"]["ID"]="cardano-node-"+node_name
    cardano_node["Job"]["TaskGroups"][0]["Tasks"][0]["Constraints"][0]["RTarget"]=node_name_constraint
    cardano_node["Job"]["TaskGroups"][0]["Name"]="cardano-node-"+node_name
    cardano_node["Job"]["TaskGroups"][0]["Tasks"][0]["Name"]="cardano-node-"+node_name

    job_defn="./jobs/"+cardano_node["Job"]["Name"]+'.json'
    job_defn=job_defn.replace(" ","_")
    with open(job_defn, 'w') as file:
        json.dump(cardano_node, file)

    output=n.job.register_job(cardano_node["Job"]["Name"], cardano_node)
    
    print(output)
    return output