import sys
import time
import logging
import os
import nomad
import json
import subprocess



n = nomad.Nomad(host="172.16.100.10", timeout=10)

uclnlp=json.load(open("./jobs/uclnlp.json","r"))
binary_classification=json.load(open("./jobs/binary_classification.json","r"))
news_score=json.load(open("./jobs/news_score.json","r"))
cardano_node=json.load(open("./jobs/cardano_node_passive.json","r"))


def total_allocs(nodeName):
    jobs=n.jobs.get_jobs()
    running_allocs=0
    adapter_running=False
    passive_node=False
    for job in jobs:
        try:
            job_id=job["ID"]
            allocs=n.job.get_allocations(job_id)
            for alloc in allocs:
                if alloc["ClientStatus"] == "running" and alloc["NodeName"] == nodeName:
                    running_allocs+=1
                    if "adapter" in alloc["Name"] and (int(time.time())-int(alloc["CreateTime"]/(10**9))) >= 120:
                        adapter_running=True
                        try:
                            if "y" in n.job.get_job(job_id)["TaskGroups"][0]["Tasks"][0]["Env"]["cardano_passive"].lower():
                                passive_node=True
                                print("cardano node")
                                print(nodeName)
                        except Exception as e:
                            logging.error(str(e))
        except Exception as e:
            logging.error(str(e))    
    return running_allocs, adapter_running, passive_node

def total_jobs(jobName):
    jobs=n.jobs.get_jobs()
    running_allocs=0
    for job in jobs:
        try:
            job_id=job["ID"]
            allocs=n.job.get_allocations(job_id)
            for alloc in allocs:
                if alloc["ClientStatus"] == "running" and jobName in alloc["Name"]:
                    node_id=alloc["NodeID"]
                    if n.node.get_node(node_id)["Datacenter"]=="nunet-development":
                        running_allocs+=1
        except Exception as e:
            logging.error(str(e))
        
    return running_allocs


def run_jobs():
    nodes=n.nodes.get_nodes()
    uclnlp=json.load(open("./jobs/uclnlp.json","r"))
    binary_classification=json.load(open("./jobs/binary_classification.json","r"))
    news_score=json.load(open("./jobs/news_score.json","r"))
    cardano_node=json.load(open("./jobs/cardano_node_passive.json","r"))



    for node in nodes:
        #node["Datacenter"] also we can check datacenter for private-alpha
        if node["Status"]=="ready" and node["Datacenter"]=="nunet-development":
            print(node["Datacenter"])
            node_name=node["Name"]
            node_name_1=node["Name"]
            total_alloc,adapter_running,passive_node=total_allocs(node_name)
            #nodes that are running only adapter
            print(node_name)
            print(total_alloc)
            node_name=node_name.replace(" ","_")
            if adapter_running:
                print(total_alloc)
                node_res=n.node.get_node(node["ID"])["Resources"]
                memory_total=node_res["MemoryMB"]
                cpu_total=node_res["CPU"]
                node_res=n.node.get_node(node["ID"])["Reserved"]
                memory_reserved=node_res["MemoryMB"]
                cpu_reserved=node_res["CPU"]
                memory_available=memory_total-memory_reserved
                cpu_available=cpu_total-cpu_reserved
                print(memory_available)
                print(cpu_available)
                uclnlp_started=False
                binary_classification_started=False

                uclnlp_running=total_jobs("uclnlp")
                binary_classification_running=total_jobs("binary_classification")
                news_score_running=total_jobs("news_score")
                
                #cardano_node
                if passive_node and memory_available >=4000 and  cpu_available >=4000:
                    cardano_node["Job"]["Name"]="testing-cardano-node-"+node_name
                    cardano_node["Job"]["ID"]="testing-cardano-node-"+node_name
                    cardano_node["Job"]["TaskGroups"][0]["Tasks"][0]["Constraints"][0]["RTarget"]=node_name_1
                    cardano_node["Job"]["TaskGroups"][0]["Name"]="testing-cardano-node-"+node_name
                    cardano_node["Job"]["TaskGroups"][0]["Tasks"][0]["Name"]="testing-cardano-node-"+node_name

                    job_defn="./jobs/"+cardano_node["Job"]["Name"]+'.json'
                    job_defn=job_defn.replace(" ","_")
                    print("job defnition")
                    print(job_defn)
                    with open(job_defn, 'w') as file:
                        json.dump(cardano_node, file)

                    try:
                        print(job_defn)
                        output=subprocess.check_output( ["curl", "-XPUT" ,"-d" ,"@./"+str(job_defn) ,"http://nomad.nunet.io:4646/v1/jobs"])
                    except Exception as e:
                        logging.exception("message",e)

                #uclnlp                
                if memory_available >=8000 and  cpu_available >=2000:
                    
                    if uclnlp_running==0 or uclnlp_running <= binary_classification_running-1  or uclnlp_running <= news_score_running-1:
                    
                        uclnlp["Job"]["Name"]="testing-uclnlp-"+node_name
                        uclnlp["Job"]["ID"]="testing-uclnlp-"+node_name
                        uclnlp["Job"]["TaskGroups"][0]["Name"]="testing-uclnlp-"+node_name
                        uclnlp["Job"]["TaskGroups"][0]["Tasks"][0]["Name"]="testing-uclnlp-"+node_name
                        uclnlp["Job"]["TaskGroups"][0]["Tasks"][0]["Constraints"][0]["RTarget"]=node_name_1
                        job_defn="./jobs/"+uclnlp["Job"]["Name"]+'.json'
                        with open(job_defn, 'w') as file:
                            json.dump(uclnlp, file)
                    
                        try:
                            print(job_defn)
                            output=subprocess.check_output( ["curl", "-XPUT" ,"-d" ,"@./"+str(job_defn) ,"http://nomad.nunet.io:4646/v1/jobs"])
                            uclnlp_started=True
                        except Exception as e:
                            logging.exception("message",e)

                #binary
                if memory_available >= 8000 and  cpu_available >=2000 :
                    if binary_classification_running==0 or (binary_classification_running <= news_score_running-1 and uclnlp_started==False) :
                        binary_classification["Job"]["Name"]="testing-binary-classification-"+node_name
                        binary_classification["Job"]["ID"]="testing-binary-classification-"+node_name
                        binary_classification["Job"]["TaskGroups"][0]["Tasks"][0]["Constraints"][0]["RTarget"]=node_name_1                
                        binary_classification["Job"]["TaskGroups"][0]["Name"]="testing-binary-classification-"+node_name
                        binary_classification["Job"]["TaskGroups"][0]["Tasks"][0]["Name"]="testing-binary-classification-"+node_name
                        job_defn="./jobs/"+binary_classification["Job"]["Name"]+'.json'
                        job_defn=job_defn.replace(" ","_")
                        with open(job_defn, 'w') as file:
                            json.dump(binary_classification, file)
                        try:
                            print(job_defn)
                            output=subprocess.check_output( ["curl", "-XPUT" ,"-d" ,"@./"+str(job_defn) ,"http://nomad.nunet.io:4646/v1/jobs"])
                            binary_classification_started=True
                        except Exception as e:
                            logging.exception("message",e)
                #news_score
                if news_score_running==0 or memory_available >=4000 and  cpu_available >=2000 and uclnlp_started==False and binary_classification_started==False:
                    news_score["Job"]["Name"]="testing-news-score-"+node_name
                    news_score["Job"]["ID"]="testing-news-score-"+node_name
                    news_score["Job"]["TaskGroups"][0]["Tasks"][0]["Constraints"][0]["RTarget"]=node_name_1
                    news_score["Job"]["TaskGroups"][0]["Name"]="testing-news-score-"+node_name
                    news_score["Job"]["TaskGroups"][0]["Tasks"][0]["Name"]="testing-news-score-"+node_name

                    job_defn="./jobs/"+news_score["Job"]["Name"]+'.json'
                    job_defn=job_defn.replace(" ","_")
                    with open(job_defn, 'w') as file:
                        json.dump(news_score, file)

                    try:
                        print(job_defn)
                        output=subprocess.check_output( ["curl", "-XPUT" ,"-d" ,"@./"+str(job_defn) ,"http://nomad.nunet.io:4646/v1/jobs"])
                    except Exception as e:
                        logging.exception("message",e)

