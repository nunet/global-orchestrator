import grpc
import service_pb2_grpc
import service_pb2
import pickle


def run():
    with grpc.insecure_channel('localhost:4869') as channel:
        stub = service_pb2_grpc.RegistryStub(channel)
        response = stub.reqServiceEndpoints(
            service_pb2.ServiceRequest(service_name=""))
        endpoints = pickle.loads(response.endpoints)
    
        print(endpoints)
    
        # stub = service_pb2_grpc.RegistryStub(channel)
        # response = stub.runService(
        #     service_pb2.ServiceDesc(user_name="new-user",service_name="cardano_relay_node"))

        # print(response)


if __name__ == '__main__':
    run()
